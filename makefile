COMPILER = gcc
CFLAGS = -Wall -pedantic -lcrypto -lm

EXES = parallel_search_keyspace

all: ${EXES}

parallel_search_keyspace:  parallel_search_keyspace.c
	${COMPILER} ${CFLAGS}  parallel_search_keyspace.c -o parallel_search_keyspace
clean:
	rm -f *~ *.o ${EXES}
