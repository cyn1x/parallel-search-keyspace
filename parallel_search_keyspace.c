/*
 * Parallel search algorithm that finds a full key for a given partial key.
 * Author: Zac Vukovic (zmadden@myune.edu.au)
 *
 * The purpose of this program is to brute force a partial key to find the rest
 * of the key to decrypt a cipher. That decrypted cipher is then used to
 * compare a plaintext file for equality. If equal, the key is assumed to be
 * discovered.
 *
 * A partial key for this example will be: 4+zxQ8dwmYVcH+dznll3BpSR3DyX+Z4.
 * Removing 3 bytes gives: 4+zxQ8dwmYVcH+dznll3BpSR3DyX. The program takes the
 * following arguments:
 *
 * - parallel_search_keyspace [number of processes] [partial key]
 * - [number of processes]: the number of child processes the parent process
 * will spawn for the partial key search.
 * - [partial key]: The partial key to be found.
 *
 * Example usage:
 * $ make
 * $ parallel_search_keyspace 5 4+zxQ8dwmYVcH+dznll3BpSR3DyX
 *
 * Example output:
 * Plaintext: This is my super secret text
 * Ciphertext: ▒▒.▒t▒▒=z▒▒h▒▒▒▒w▒▒AuDf▒▒^
 *
 * Successful key search by process 2, with pid: 228569
 * OK: enc/dec ok for "This is my super secret text"
 * Key No.:727331840: Sending to shared memory
 *
 * Proccess 1 with pid 228569 is terminating
 *
 * Parent received the full key: 4+zxQ8dwmYVcH+dznll3BpSR3DyX+Z4
 *
 * Terminating any remaining child processes, please wait...
 * Proccess 3 with pid 228571 is terminating
 * Proccess 4 with pid 228572 is terminating
 * Proccess 5 with pid 228573 is terminating
 * Proccess 2 with pid 228570 is terminating
 * $
 */

#include "parallel_search_keyspace.h"

/**
 * handler() - Toggles the signal flag for all child processes to stop working.
 * @sig:    Signal flag for child processes to check during the brute force
 *          algorithm.
 */
void handler(int sig)
{
        if (sig == SIGUSR1)
                stop_signal = 1;
}

/**
 * main() - Program entry point.
 * @argc:   Number of arguments given
 * @argv:   Arguments given to the program
 *
 * Return:  0.
 */
int main(int argc, char **argv)
{
        signal(SIGUSR1, handler);

        int valid;

        struct program_data_properties *pdp;
        struct file_data_properties *fdp;
        struct key_data_properties *kdp;

        pdp = malloc(sizeof(struct program_data_properties));
        fdp = malloc(sizeof(struct file_data_properties));
        kdp = malloc(sizeof(struct key_data_properties));

        valid = parse_args(pdp, argc, argv);

        if (valid < 0) {
                exit(EXIT_FAILURE);
        } else {
                initialise_data(pdp, kdp);
                prepare_data_types(pdp, fdp, kdp);
                run_program(pdp, fdp, kdp);
        }

        clean(pdp, fdp, kdp);

        return 0;
}

/**
 * parse_args() - Check validity of program arguments.
 * @pdp:    Program data properties relevant to the whole scope of the program
 * @argc:   Number of arguments passed to the program
 * @argv:   The arguments given to the program
 *
 * Return:  0.
 */
int parse_args(struct program_data_properties *pdp, int argc, char **argv)
{
        if (argc != REQUIRED_ARGS) {
                printf("Error: Invalid argument. %i argument(s) "
                       "entered; %i arguments required.\n",
                       argc, REQUIRED_ARGS);
                printf("Usage: Specify the number of child processes "
                       "and the partial key.\n");
                return -1;
        }
        if (atoi(argv[1]) < 1) {
                printf("Error: Invalid argument. %s is not a valid "
                       "number of processes.\n",
                       argv[1]);
                printf("Usage: Specify at least 1 process to run the "
                       "program.\n");
                return -1;
        }

        pdp->max_key_len = MAX_KEY_LEN;
        pdp->n_proc = atoi(argv[1]);
        pdp->partial_key = argv[2];

        return 0;
}

/**
 * initialise_data() - Initialises structure data for use.
 * @pdp:    Program data properties.
 * @kdp:    Key data properties.
 */
void initialise_data(struct program_data_properties *pdp,
                     struct key_data_properties *kdp)
{
        kdp->key_data = (unsigned char *) pdp->partial_key;
        kdp->key_data_len = strlen(pdp->partial_key);
        kdp->key_len = MAX_KEY_LEN;
        kdp->trial_key_len = MAX_KEY_LEN;
        kdp->decrypt_ctx = EVP_CIPHER_CTX_new();
}

/**
 * prepare_data_types() - Processes structure data and prepares data types.
 * @fdp:   File data properties.
 * @kdp:   Key data properties.
 */
void prepare_data_types(struct program_data_properties *pdp,
                        struct file_data_properties *fdp,
                        struct key_data_properties *kdp)
{
        fdp->cipher_in = read_file(CIPHER_FILE, &fdp->cipher_len);
        fdp->plain_in = read_file(PLAINTEXT_FILE, &fdp->plaintext_len);

        printf("\nPlaintext: %s\n", fdp->plain_in);
        printf("Ciphertext: %s\n", fdp->cipher_in);

        // Condition known portion of key
        // Only use most significant 32 bytes of data if > 32 bytes
        if (kdp->key_data_len > pdp->max_key_len)
                kdp->key_data_len = pdp->max_key_len;

        kdp->plaintext = malloc(fdp->cipher_len * sizeof(char));

        kdp->key = calloc(kdp->key_len, sizeof(unsigned char));
        kdp->trial_key = calloc(kdp->trial_key_len, sizeof(unsigned char));

        copy_buffer(kdp->key_data, (unsigned char *) kdp->key,
                    kdp->key_data_len);
        copy_buffer(kdp->key_data, (unsigned char *) kdp->trial_key,
                    kdp->key_data_len);
}

/**
 * run_program() - Runs the program and passes structure data down the stack.
 * @pdp:    Program data properties.
 * @fdp:    File data properties.
 * @kdp:    Key data properties.
 *
 * Return:  OK.
 */
int
run_program(struct program_data_properties *pdp,
            struct file_data_properties *fdp, struct key_data_properties *kdp)
{
        unsigned long klb;

        klb = init_search_space((const unsigned char *) kdp->key, kdp->key_len,
                                kdp->key_data_len);

        pdp->klb = klb;

        determine_search_space(pdp, fdp, kdp);

        return 0;
}

/**
 * read_file() - Reads a file.
 * @path:   The path of the file.
 * @len:    The length of the buffer.
 *
 * The @len pointer is modified to reflect the length of the file.
 *
 * Return:  Buffer with file contents.
 */
unsigned char *read_file(const char *path, unsigned int *len)
{
        unsigned char *buf;

        FILE *fp;
        fp = fopen(path, "r");

        if (!fp) {
                printf("\n\nFile: %s ", path);
                fflush(stdout);
                perror("could not be opened");
        }

        fseek(fp, 0, SEEK_END);
        *len = ftell(fp);
        rewind(fp);
        buf = calloc(*len + 1, sizeof(unsigned char));
        buf[*len] = '\0';
        fread(buf, *len, 1, fp);
        fclose(fp);

        return buf;
}

/**
 * copy buffer() - Copies contents from a buffer to another.
 * @src:    Source buffer.
 * @dest:   Destination buffer.
 * @len:    The length of both buffers
 */
void copy_buffer(const unsigned char *src, unsigned char *dest,
                 const unsigned int len)
{
        for (int i = 0; i < len; i++)
                dest[i] = src[i];
}

/**
 * init_search_space() - Initialises the search space.
 * @key:    The known key.
 * @kl:     The maximum key length to solve.
 * @kdl:    The known key's length.
 *
 * Return:  Missing bits from @key to be used in the search.
 */
unsigned long init_search_space(unsigned const char *key, const unsigned int kl,
                                const unsigned int kdl)
{
        unsigned int mb;
        unsigned long klb = 0;

        mb = kl - kdl;

        for (unsigned int i = 0; i < mb; i++)
                klb = ((unsigned long) (key[kl - mb + i] & 0xFFFFu)
                        << ((mb - i) * 8));

        return klb;
}

/**
 * determine_search_space() - Creates child processes.
 * @pdp:    Program data properties.
 * @fdp:    File data properties.
 * @kdp:    Key data properties.
 *
 * Child processes are created and the parent process logs their process
 * numbers. The child processes and parent process diverge to complete
 * different tasks.
 *
 * Return:  OK.
 */
int determine_search_space(struct program_data_properties *pdp,
                           struct file_data_properties *fdp,
                           struct key_data_properties *kdp)
{
        void *sm = create_shared_memory(128);
        char key[256];
        int key_found;

        pid_t pid;

        pdp->pid_list = calloc(pdp->n_proc, sizeof(pid_t));

        for (int i = 1; i <= pdp->n_proc; ++i) {
                if ((pid = fork()) < 0) {
                        perror("Fork error");
                        exit(EXIT_FAILURE);
                }
                if (pid == 0) {
                        key_found = calculate_permutations(pdp, fdp, kdp, i);

                        if (key_found) {
                                sprintf(key, "%s",
                                        (unsigned char *) kdp->trial_key);

                                /*
                                 * No locking is being used. Only one process
                                 * is able to write to the shared memory, and
                                 * this is only done when the full key is
                                 * found.
                                 *
                                 * The maximum search space for each process
                                 * does not overlap with other processes. Thus,
                                 * only one process can write to shared memory.
                                 *
                                 * It would be computationally and physically
                                 * inefficient to add a semaphore structure
                                 * to perform a check that isn't really needed.
                                 * */
                                memcpy(sm, key, sizeof(key));

                                clean(pdp, fdp, kdp);
                                printf("\nProccess %i with pid %i is "
                                       "terminating\n",
                                       i,
                                       getpid());
                                exit(EXIT_SUCCESS);
                        }
                        clean(pdp, fdp, kdp);
                        printf("Proccess %i with pid %i is "
                               "terminating\n", i,
                               getpid());
                        exit(EXIT_FAILURE);
                } else {
                        pdp->pid_list[i - 1] = pid;
                }
        }

        process_manager(pdp, sm);

        return 0;
}

/**
 * calculate_permutations() - Allocates different permutations for children.
 * @pdp:    Program data properties.
 * @fdp:    File data properties.
 * @kdp:    Key data properties.
 * @cid:    The child program index number.
 *
 * Determines the maximum range of bits to decrypt. Each iteration assesses
 * a combination of the permutations available for decryption and then
 * comparison with the plaintext file. The maximum space is divided by the
 * number of processes, and each process is allocated their own section to
 * search.
 *
 * The loop follows three major steps; fills missing bytes with data from
 * the search space, decryptes the trial key and modifiers the plaintext
 * pointer, then checks if the decrypted plaintext matches the plaintext file.
 *
 * Return:  True or False.
 */
int calculate_permutations(struct program_data_properties *pdp,
                           struct file_data_properties *fdp,
                           struct key_data_properties *kdp, unsigned int cid)
{
        unsigned char *tk;
        unsigned long ms, min, max;
        unsigned int mb;
        double div;

        tk = (unsigned char *) kdp->trial_key;
        mb = kdp->key_len - kdp->key_data_len;

        ms = ((unsigned long) 1
                << ((kdp->trial_key_len - kdp->key_data_len) * 8)) -
             1;
        div = ceil((double) ms / ((double) pdp->n_proc));
        min = (unsigned long) ((cid - 1) * div);
        max = (unsigned long) (cid * div) - 1;

        for (unsigned long counter = max;
             counter > min && !stop_signal; counter--) {
                unsigned long tlb = pdp->klb | counter;

                determine_missing_bytes(kdp, tk, tlb, mb);

                calculate_plaintext(fdp, kdp, tk);

                if (match_key(fdp, kdp->plaintext, cid, counter))
                        return true;

                memset(kdp->plaintext, 0, fdp->cipher_len * sizeof(char));
        }

        return false;
}

/**
 * allocate_missing_bytes() - Packs the trial key with the missing bytes.
 * @kdp:    Key data properties.
 * @tk:     Trial key.
 * @tlb:    Trial low bits
 * @mb:     Missing bytes
 *
 * Determines the next combination of bytes from the permutations.
 */
void determine_missing_bytes(struct key_data_properties *kdp, unsigned char *tk,
                             unsigned long tlb, unsigned int mb)
{
        for (unsigned int i = 0; i < mb; i++)
                tk[kdp->key_len - mb + i] = (unsigned char) (tlb
                        >> ((mb - i - 1) * 8));
}

/**
 * calculate_plaintext() - Attempts to decrypt the plaintext.
 * @pdp:    Program data properties.
 * @fdp:    File data properties.
 * @tk:     Trial key.
 * @pt:     Plaintext.
 *
 * Initialise data types for AES decryptions and tests the permutation.
 * Modifies the plaintext pointer from @kdp with the
 */
void calculate_plaintext(struct file_data_properties *fdp,
                         struct key_data_properties *kdp, unsigned char *tk)
{
        if (aes_init(kdp->decrypt_ctx, (unsigned char *) tk,
                     kdp->trial_key_len) <
            0) {
                printf("Couldn't initialize AES cipher\n");
                exit(EXIT_FAILURE);
        }

        aes_decrypt(kdp->decrypt_ctx, (unsigned char *) fdp->cipher_in,
                    kdp->plaintext, (int) fdp->cipher_len);
        kdp->plaintext[fdp->plaintext_len] = '\0';
}


/**
 * aes_init() - Verifies if the decryption data is correct.
 * @d_ctx:          Decryption context.
 * @key_data:       Key to verify.
 * @key_data_len:   Length of the key.
 *
 * Return:  OK or ERR.
 */
int aes_init(EVP_CIPHER_CTX *d_ctx, unsigned char *key_data,
             unsigned int key_data_len)
{
        unsigned char key[MAX_KEY_LEN], iv[MAX_KEY_LEN];

        if (key_data_len > MAX_KEY_LEN) key_data_len = MAX_KEY_LEN;

        copy_buffer(key_data, key, key_data_len);
        copy_buffer(key_data, iv, key_data_len);

        EVP_CIPHER_CTX_init(d_ctx);
        if (EVP_DecryptInit_ex(d_ctx, EVP_aes_256_cbc(), NULL, key, iv)
            < 1)
                return -1;

        return 0;
}

/**
 * aes_decrypt() - The decryption process.
 * @e_ctx:          Encryption context.
 * @ciphertext:     Ciphertext file to decrypt.
 * @len:            Length of the ciphertext file.
 */
void aes_decrypt(EVP_CIPHER_CTX *e_ctx, unsigned char *cipher, char *plaintext,
                 int len)
{
        int p_len = len, f_len = 0;

        EVP_DecryptInit_ex(e_ctx, NULL, NULL, NULL, NULL);
        EVP_DecryptUpdate(e_ctx, (unsigned char *) plaintext, &p_len, cipher,
                          len);
        EVP_DecryptFinal_ex(e_ctx, (unsigned char *) plaintext + p_len,
                            &f_len);
}

/**
 * match_key() - Check if decrypted key matches the plaintext.
 * @fdp:    File data properties.
 * @pt:     Plaintext.
 * @cid:    The child program index number.
 * @cnt:    Current count of the iteration.
 *
 * Compares the decrypted ASCII with the original plaintext.
 *
 * Return:  True if there is a match, otherwise False.
 */
bool match_key(struct file_data_properties *fdp, char *pt, unsigned int cid,
               unsigned long cnt)
{
        if (strncmp(pt, (char *) fdp->plain_in, fdp->plaintext_len) == 0) {
                printf("\nSuccessful key search by process %i, with "
                       "pid: %i",
                       cid + 1,
                       getpid());
                printf("\nOK: enc/dec ok for \"%s\"\n", pt);
                printf("Key No.:%lu: Sending to shared memory\n", cnt);

                return true;
        }

        return false;
}

/**
 * process_manager() - Waits for child processes to complete.
 * @pdp:    Program data properties.
 * @sm:     Shared memory.
 *
 * Waits for a successful exit status from a child process. A successful exit
 * means the key has been found.
 *
 */
void process_manager(struct program_data_properties *pdp, void *sm)
{
        int status;

        for (int i = 1; i < pdp->n_proc + 1; i++) {
                wait(&status);

                if (WEXITSTATUS(status) == EXIT_SUCCESS) {
                        printf("\nParent received the full key: %s\n",
                               (char *) sm);
                        printf("\nTerminating any remaining child "
                               "processes, please wait...\n");

                        terminate_children(pdp);
                        return;
                }
        }
}

/**
 * terminate_children() - Terminates remaining child processes.
 * @pdp:    Program data properties.
 *
 * Terminates any remaining children once the key has been found by sending
 * a user-defined signal.
 */
void terminate_children(struct program_data_properties *pdp)
{
        for (int i = 0; i < pdp->n_proc; i++) {
                kill(pdp->pid_list[i], SIGUSR1);
        }
}

/**
 * create_shared_memory() - Creates shared memory.
 * @size:   The size of the memory to be allocated.
 *
 * The protection is set to be readable and writable for all processes. The
 * visibility is shared between all processes by MAP_SHARED, though not
 * external third-party processes by MAP_ANONYMOUS.
 *
 * Return:  A new mapping in the virtual address space of the calling process.
 */
void *create_shared_memory(size_t size)
{
        int protection, visibility;

        protection = PROT_READ | PROT_WRITE;
        visibility = MAP_SHARED | MAP_ANONYMOUS;

        return mmap(NULL, size, protection, visibility, -1, 0);
}

/**
 * clean() - Cleans allocated memory.
 * @pdp:    Program data properties.
 * @fdp:    File data properties.
 * @kdp:    Key data properties.
 *
 * TODO:    Memory manager.
 *
 */
void
clean(struct program_data_properties *pdp, struct file_data_properties *fdp,
      struct key_data_properties *kdp)
{
        EVP_CIPHER_CTX_cleanup(kdp->decrypt_ctx);
        EVP_CIPHER_CTX_free(kdp->decrypt_ctx);

        free(pdp->pid_list);
        free(pdp);
        free(kdp->plaintext);
        free(kdp->key);
        free(kdp->trial_key);
        free(kdp);
        free(fdp->cipher_in);
        free(fdp->plain_in);
        free(fdp);
}
