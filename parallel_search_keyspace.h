#ifndef PARALLEL_SEARCH_SPACE_H
#define PARALLEL_SEARCH_SPACE_H

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <math.h>
#include <openssl/evp.h>
#include <openssl/aes.h>

#define REQUIRED_ARGS 3
#define MAX_KEY_LEN 32
#define PLAINTEXT_FILE "plain.txt"
#define CIPHER_FILE "cipher.txt"

volatile int stop_signal = 0; // Used for signalling processes to terminate.

/**
 * program_data_properties - Data relevant to the entire program scope.
 * @pid_list:       Pointer to child processes for signalling purposes.
 * @mak_key_len:    Maximum key length to search.
 * @n_proc:         Number of child processes.
 * @klb:            Key low bits from the missing low end bites of the key to
 *                  the missing end bits of the key.
 * @partial_key:    Pointer to partial key given as an argument.
 */
struct program_data_properties {
    pid_t *pid_list;
    unsigned int max_key_len, n_proc;
    unsigned long klb;
    char *partial_key;
};

/**
 * struct file_data_properties - Relevant cipher and plaintext file properties.
 * @cipher_in:  Pointer to bytes from the cipher file.
 * @plain_in:   Pointer to bytes from the plaintext file.
 * @cipher_len: Length of the cipher file.
 * @plain_len:  Lngth of the plaintext file.
 */
struct file_data_properties {
    unsigned char *cipher_in, *plain_in;
    unsigned int cipher_len, plaintext_len;
};

/**
 * struct key_data_properties - Decryption key properties.
 * @plaintext:      Reusable plaintext context.
 * @key:            Padded with maximum search space bytes.
 * @key_data:       Partial key given as an argument.
 * @trial_key:      Changes every permutation for decryption.
 * @key_len:        Length of the key.
 * @key_data_len:   Length of the partial key.
 * @trial_key_len:  Length of the trial key.
 * @decrypt_ctx:    Reusable decryption context.
 */
struct key_data_properties {
    char *plaintext;
    unsigned char *key, *key_data, *trial_key;
    unsigned int key_len, key_data_len, trial_key_len;
    EVP_CIPHER_CTX *decrypt_ctx;
};

int parse_args(struct program_data_properties *pdp, int argc, char **argv);

void initialise_data(struct program_data_properties *pdp,
                     struct key_data_properties *kdp);

void prepare_data_types(struct program_data_properties *pdp,
                        struct file_data_properties *fdp,
                        struct key_data_properties *kdp);

int run_program(struct program_data_properties *pdp,
                struct file_data_properties *fdp,
                struct key_data_properties *kdp);

unsigned char *read_file(const char *filepath, unsigned int *len);

void
copy_buffer(const unsigned char *src, unsigned char *dest, unsigned int len);

unsigned long
init_search_space(unsigned const char *key, unsigned int kl, unsigned int kdl);

int determine_search_space(struct program_data_properties *pdp,
                           struct file_data_properties *fdp,
                           struct key_data_properties *kdp);

int calculate_permutations(struct program_data_properties *pdp,
                           struct file_data_properties *fdp,
                           struct key_data_properties *kdp, unsigned int cid);

void calculate_plaintext(struct file_data_properties *fdp,
                         struct key_data_properties *kdp, unsigned char *tk);

int aes_init(EVP_CIPHER_CTX *d_ctx, unsigned char *key_data,
             unsigned int key_data_len);

void aes_decrypt(EVP_CIPHER_CTX *e_ctx, unsigned char *cipher, char *plaintext,
                 int len);

void determine_missing_bytes(struct key_data_properties *kdp, unsigned char *tk,
                             unsigned long tlb, unsigned int mb);

bool match_key(struct file_data_properties *fdp, char *pt, unsigned int cid,
               unsigned long cnt);

void process_manager(struct program_data_properties *pdp, void *sm);

void terminate_children(struct program_data_properties *pdp);

void *create_shared_memory(size_t size);

void
clean(struct program_data_properties *pdp, struct file_data_properties *fdp,
      struct key_data_properties *kdp);

#endif
